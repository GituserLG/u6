package imageFilter;

/**
 * 
 * @author Florian Kadner
 * @version 1.0 20170913
 */
public class Filter {

	public Filter() {

	}

	/**
	 * This method implements a linear filter with discrete convolution
	 * 
	 * @param image
	 *            the image which should be filtered
	 * @param kernel
	 *            the kernel used for the linear filter
	 * @return the filtered image
	 * 
	 * @param i 
	 *       pixel in der i-ten Zeile
	 * 
	 * @param j
	 *       pixel in der j-ten Zeile
	 * @param heigth
	 *       H�he des Bildes
	 * @param weigth
	 *       Breite des Bildes
	 * @param x
	 * 
	 * @param y
	 * 
	 * @param a1
	 *     x-Koordiante des Kernelmittelpunkts
	 * @param a2
	 *     y-Koordinate des Kernelmittelpunkts
	 * @param sum
	 */
	public Image linearFilter(Image image, float[][] kernel) {

		Image filtered = image.clone();
		int i = 0;
		int j = 0;
		int height = image.getHeight();
		int width = image.getWidth();
		int n = kernel.length;
 		int a1 = Math.round( n/2);
		int a2 = Math.round(n/2);
		int x = 0;
		int y = 0;
		float sum = 0;
		int border1 = i + x - a1;
		int border2 = j + y - a2;
		
		
		if (border1  < 0 ) {
			return 0}
		if (border1  > height) {
			return height }
		else border1  
		}
								
		if(border2 < 0) {
			return 0 }
		if (border > height) {
			return height }
		else border2
		

		while(i <=  height || j <= width) {
			while(x <= n || y <= n){ 
						sum += ((image.getPixel( border1, border2)) * kernel[x][y]); 
						if(x == n) {y++; x = 0;} {x++;}}
			
			
			if(i == height) {j++; i = 0;}{i++;}  {filtered.setPixel(sum, i, j);}}  
			
		
		

			return filtered;
		}

	
	/**
	 * This method implements a nonlinear filter with median filter
	 * 
	 * @param image
	 *            the image which should be filtered
	 * @param filtersize
	 *            the size of the median filter
	 * @return the filtered image
	 * @param i
	 *     Pixel in der i-ten Zeile
	 * @param j
	 *     Pixel in der j-ten Zeile
	 * @param y
	 *      y-Koordinate
	 * @param x 
	 *      x-Koordinate
	 * @param medians
	 *      Median Werte
	 * @param k
	 */     neuer Median Wert
	public Image nonLinearFilterMedian(Image image, int filtersize) {

		Image filtered = image.clone();
		int i = 0;
		int j = 0;
		int y = 0;
		int x = 0;
		float medians = 0;
		float k = new localNeighbours.getMedian(); 
		
		
		
		while(i <=  image.getHeight() || j <= image.getWidth()) {
		  while(x <= filtersize || y <= filtersize){ 
						image.setPixel(imageFilter.LocalNeighbours.getMedian(), i, j);
				
						if(x == filtersize ) {y++; x = 0;} {x++;}}
			
			
			if(i == image.getHeight()) {j++; i = 0;}{i++;} 
		 
		

		return filtered;
	}

}
