package imageFilter;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 
 * @author Florian Kadner
 * @version 1.0 20170913
 */
public class LocalNeighbours {

	public LocalNeighbours() {

	}

	private ArrayList<Float> pixels = new ArrayList<Float>();

	public Float[] getPixels() {
		Float[] pixelsArray = new Float[pixels.size()];
		pixelsArray = pixels.toArray(pixelsArray);
		return pixelsArray;
	}

	public int getSize() {
		return pixels.size();
	}

	public void addPixel(float Pixel) {
		pixels.add(Pixel);
	}

	/**
	 *@author Lucas Gr�schl Sabine Kopp
	 *@param beginn
	 *@param end
	 *@param i
	 *sorts the local neighbourhood with a specific sorting algorithm
	 */
	
	public void sort() {
		Float[] A = getPixels();
		int beginn = -1;
		int end = A.length; 
		int i = 0;
		
		while(beginn < end) { beginn += 1; end -= 1; 
			for(i = beginn ; i < end ; i++); 
					{if (A[i]> A[i + 1]) 
						{swap(A[i],A[i + 1]);}};
					
			for(i = end ; beginn <= i--;)
				{if (A[i] > A[i + 1]) {swap(A[i],A[i + 1]);}}
			}
					
			this.pixels = new ArrayList<>(Arrays.asList(A));

	}	
	/**
	 * @author Lucas Gr�schl Sabine Kopp
	 * @param x
	 *     x-Wert
	 * @param y
	 *     y-Wert
	 */
	public void swap(float x,float y) {
		float temp = x;     
			y = x;
		 y = temp; 
		}

	/**
	 * @author Lucas Gr�schl Sabine Kopp 
	 * @param median 
	 *       Median Wert
	 * @param ArrayList
	 *       List of Arrays
	 * @return returns the median of the current local neighbourhood
	 */
	public float getMedian() {
		
		float median = -77.0f;
		
			
		float ArrayList;
		Arrays.sort(ArrayList);
		
		if (ArrayList.length % 2 == 0) 
			median = ((float)ArrayList[ArrayList.length/2]) + (float)ArrayList[ArrayList.length/2 -1])/2;
		else 
			median = (float) ArrayList[ArrayList.length/2]; 

		
		if (this.pixels.size()% 2 == 0)
			median = this.pixels.size()/ 2 + (this.pixels.size()/2 -1)/2;
		else
			median = (this.pixels.size()+1)/2; 

		
		return median;

	}

}
