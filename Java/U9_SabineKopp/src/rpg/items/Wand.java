package rpg.items;

/**
 * 
 * @author Sabine Kopp, Lucas Gr�schl
 *
 */

public class Wand extends Item {
	private String name;{
		String Wand = name;}
	
	private int attackValue;{
		this.attackValue = 2;
	}
	private int defenseValue;{
		this.defenseValue = 0;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAttackValue() {
		return attackValue;
	}

	public int getDefenseValue() {
		return defenseValue;
	}

}
