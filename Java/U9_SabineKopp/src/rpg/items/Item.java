package rpg.items;
/**
 * 
 * @author Sabine Kopp, Lucas Gr�schl
 *
 */

public abstract class Item { 
	
	private String name;
	private int attackValue;
	private int defenseValue;
	
	/**
	 * 
	 * @return returns name of the item
	 */
	
	public String getName() {
		return name;
	}
	/**
	 * 
	 * @return returns value of attack 
	 */
	public int getAttackValue() {
		return attackValue;
	}
	
	/**
	 * 
	 * @return returns value of defense
	 */

	public int getDefenseValue() {
		return defenseValue;
	}
	
	/**
	 * 
	 * @return returns new items if the player get some new ones
	 */

	public static Item[] getAllItems() {
		 return new Item[] {null, new Sword(), new Armor(), new Wand()};
	}

}
