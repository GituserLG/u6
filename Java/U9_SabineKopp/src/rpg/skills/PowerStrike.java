package rpg.skills;

import rpg.characters.*;

//import rpg.items.Sword;

public class PowerStrike extends Skill{

	private String name;
	String PowerStrike = name;

	private RpgCharacter RpgCharacter;{
	}
	private  int mpCost;{
		this.mpCost = 10;
	}
	
	public String getName(){
		return name;
	}
	
	public RpgCharacter getRpgCharacter() {
		return RpgCharacter;
	}
	
	public int getMpCost() {
		return mpCost;
	}
	
	public boolean testChracterClass()
	{
		if ( RpgCharacter.getClass().toString().equals("Warrior") && RpgCharacter.getItem().getClass().toString().equals("Sword")) {
					
			return true; 
		}
		
		return false;
	}
	
	public void learnSkill() 
	{
		if(testChracterClass()) {
			
		}
	}
	public void use(RpgCharacter enemy) {
		int AD = 2 * RpgCharacter.getAttackValue();
		
		enemy.receiveNormalDamage(AD);
	}
}

