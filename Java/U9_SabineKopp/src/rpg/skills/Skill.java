package rpg.skills;

import rpg.characters.*;

public abstract class Skill {
	private String name;
	private RpgCharacter RpgCharacter;
	private  int mpCost;
	
	public String getName(){
		return name;
	}
	
	public RpgCharacter getRpgCharacter() {
		return RpgCharacter;
	}
	
	public int getMpCost() {
		return mpCost;
	}
	
	public static Skill[] getAllSkills(RpgCharacter rpgChar) {
		  if (rpgChar.toString().equals("Mage")) return new Skill[] {null ,  new Fire()} ;
		  return new Skill[] { null , new PowerStrike()};                   
	}
	public abstract void use(RpgCharacter enemy); 
}