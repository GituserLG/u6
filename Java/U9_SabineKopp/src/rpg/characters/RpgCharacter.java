package rpg.characters;

import rpg.items.Item;
import rpg.skills.Skill;
/**
 * 
 * @author Sabine Kopp, Lucas Gr�schl
 *
 */


public abstract class RpgCharacter implements ICharacter {
	private String RpgClass;
	private int maxLifePoints;
	private int actualLifePoints;
	private int maxMagicPoints;
	private int actualMagicPoints;
	private int attackValue;
	private int defenseValue;
	private boolean alive = true;
	private Item item;
	private Skill skill;
	
	/**
	 * 
	 * @param rpgclass returns the role player
	 * @param hp value of the maximum or actual life points
	 * @param mp value of the maximum or actual magic points
	 * @param atk value of the attack
	 * @param def value of the defense
	 */
	
	public RpgCharacter(String rpgclass, int hp , int mp,int atk,int def){	 
		this.maxLifePoints = hp;
		this.actualLifePoints = hp;
		this.maxMagicPoints = mp;
		this.actualMagicPoints = mp;
		this.attackValue = atk;
		this.defenseValue = def;
		this.RpgClass = rpgclass;
	}

	/**
	 * 
	 * @return returns the rpg class
	 */
	public String getRpgClass () {
		return RpgClass;
	}
	/**
	 * 
	 * @return returns the maximum of life points
	 */
		public int getMaxHp () {
		return maxLifePoints; 
	}
		
	/**
	 * 
	 * @return returns the actual value of life points
	 */
		
	public int getCurrentHp () {
		return actualLifePoints;
	}
	
	 /**
	  * 
	  * @return returns the maximum of magic points
	  */
	
	public int getMaxMp () {
		return maxMagicPoints;
	}
	
	/**
	 * 
	 * @return returns the actual value of magic points
	 */
	
	public int getCurrentMp () {
		return actualMagicPoints;
	}
	
	/**
	 * 
	 * @return returns the value of the attack
	 */
	
	public int getAttackValue () {
		return attackValue;
	}
	
	/**
	 * 
	 * @return returns the value of the defense
	 */
	
	public int getDefenseValue () {
		return defenseValue;
	}
	
	/**
	 * 
	 * @return returns status of the player
	 */
	
	public boolean getAlive() {
		return alive;
	}
	/*
	public Item setItem() {
		
	}*/
	
	/**
	 * 
	 * @return returns the item
	 */
	
	public Item getItem() {
		return item;
	}
	/*
	public Skill setSkill() {
		
	}*/
	
	/**
	 * 
	 * @return returns the skill
	 */
	
	public Skill getSkill() {
		return skill;
	}
/*
 *public getAttack(){
 *}
 * 
 */
/**
 * @return returns the full Attack Value	
 */
	public int getAttack() {
		int normalAD = getAttackValue();
		int itemAD = getItem().getAttackValue();
		int fullAD = normalAD + itemAD;
		
		return fullAD;
	}
	
	/*
	 * (non-Javadoc)
	 * @see rpg.characters.ICharacter#getDefense()
	 * 
	 * public getDefense(){
	 * }
	 */
	
	/**
	 * @author Lucas Gr�schl Sabine Kopp
	 *@return returns the full Defense Value
	 *@param nomalDef
	 *@param itemDef
	 *@param fullDef
	 */
	
	public int getDefense() {
		int normalDef = getDefenseValue();
		int itemDef = getItem().getDefenseValue();
		int fullDef = normalDef + itemDef;
		
		return fullDef;	
	}
	/*
	 * (non-Javadoc)
	 * @see rpg.characters.ICharacter#receiveNormalDamage(int)
	 * public void receiveNormalDamage(int normalDamage){
	 * }
	 */
	/**
	 * checks the actual Demage and refresh alive status
	 * @author Lucas Gr�schl Sabine Kopp
	 * @param def
	 * @param HP
	 * @param AD
	 * @param demage
	 * @param newHP
	 */
	
	public void receiveNormalDamage(int normalDamage){
		int def = getDefense();
		int HP = getCurrentHp();
		int AD = normalDamage;
		int demage = AD - def;	
		
		if (demage < 0) demage = 0;
		
		int newHP = HP - demage;
		
		if (newHP <= 0)  this.alive = false;
		this.actualLifePoints = newHP;
		
		
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see rpg.characters.ICharacter#receiveMagicDamage(int)
	 * public void receiveMagicDamage(int magicDamage){
	 * }
	 */
	/**
	 * checks the actual Demage and refresh alive status
	 * @param HP
	 * @param mD
	 * @param newHP
	 * 
	 */
	public void receiveMagicDamage(int magicDamage){
		int mD = magicDamage;
		int HP = getCurrentHp();
		int newHP = HP- mD;
		
		if (newHP <= 0) this.alive = false;
		this.actualLifePoints = newHP;
	}

	public void normalAttack(RpgCharacter enemy) {
		
		int AD = getAttackValue();
		
		if (enemy.getAlive()) enemy.receiveNormalDamage(AD);
	}
	public void useSkill(RpgCharacter enemy) {
		int MP = getCurrentMp();
 		int ImP = skill.getMpCost();
		int newMP = MP - ImP;
		if (ImP < MP) {skill.use(enemy);  this.actualMagicPoints = newMP; }
	}
	/*
	 * public String getCharacterStats() {
	 * }
	 * 
	 */
	/**
	 * 
	 * @return the Stats of the Character
	 */
	public String getCharacterStats() {
		String a = getRpgClass();
		String b = String.valueOf(getMaxHp());
		String c = String.valueOf(getMaxMp());
		String d = String.valueOf(getAttackValue());
		String e = String.valueOf(getDefenseValue());
		String f = getItem().getName();
		String g = getSkill().getName();
		
		if(f == "0") f = "_";
		if(g == "0") g = "_";
		
		String abc = "Class:" + a + "HP:" +  b + "MP:" + c + "AT:" + d + "Def:" + e + "Item:" + f + "Skill:" + g ;
		return  abc; 
		
	}
	
	}

