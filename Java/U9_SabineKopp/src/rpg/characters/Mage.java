package rpg.characters;

/**
 * 
 * @author Sabine Kopp, Lucas Gr�schl
 *
 */

public abstract class Mage extends RpgCharacter{
	public Mage( int hp , int mp,int atk,int def){	 
		super("Mage",hp,mp,atk,def);
	 }
}

