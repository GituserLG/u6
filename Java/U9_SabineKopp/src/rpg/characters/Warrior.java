package rpg.characters;

/**
 * 
 * @author Sabine Kopp, Lucas Gr�schl
 *
 */

public abstract class Warrior extends RpgCharacter {
	public Warrior( int hp , int mp,int atk,int def){
		super("Warrior",hp,mp,atk,def);
	}
		
}		