public class ToBeImplemented {

    /**
     *
     * @param x1 X-Wert des ersten Mittelpunkts
     * @param y1 y-Wert des ersten Mittelpunkts
     * @param x2 x-Wert des zweiten Mittelpunkts
     * @param y2 y-Wert des zweiten Mittelpunkts
     * @param r radius beider Kreise
     * @return gibt zurück ob sich die Kreise schneiden
     */
    public static boolean intersect(float x1, float y1, float x2, float y2, float r){

        // berechnet die Distanz beider Kreismittelpunkte

        float distanceCenters = (float)Math.sqrt((float)Math.pow(x2 - x1,2)+(float)Math.pow(y2 - y1,2));

        return distanceCenters <  r * 2;
    }

    /**
     *
     * @param x1 x-Wert des ersten Mittelpunkts
     * @param y1 y-Wert des ersten Mittelpunkts
     * @param x2 x-Wert des zweiten Mittelpunkts
     * @param y2 y-Wert des zweiten Mittelpunkts
     * @param dist gegebene Distanz
     * @return gibt true zurück wenn die Kreise näher sind als der gegebene Abstand
     */
    public static boolean isCloser(float x1, float y1, float x2, float y2, float dist) {

        float distanceCenters = (float)Math.sqrt((float)Math.pow(x2 - x1,2)+(float)Math.pow(y2 - y1,2));
         return ( distanceCenters <  dist && distanceCenters > 0);


    }

    /**
     *
     * @param x x-Wert des Mittelpunkts
     * @param y y-Wert des Mittelpunkts
     * @param dirX x-Wert des Richtungsvektors
     * @param dirY y-Wert des Richtungsvektors
     * @return gibt den neuen Mittelpunkt zurück
     */
    public static float[] moveCircle(float x, float y, float dirX, float dirY) {


        return new float[]{x + dirX,y + dirY};
    }

    /**
     *
     * @param x x-Wert des Mittelpunkts
     * @param y y-Wert des Mittelpunkts
     * @param radius Radius des Kreises
     * @param width Breite der Ebene
     * @param height Höhe der Ebene
     * @param dirX x-Wert des Richtungsvektors
     * @param dirY y-Wert des Richtungsvektors
     * @return gibt neuen richtungs vektor zur�ck
     */
    public static float[] bounceOffWall(float x, float y, float radius, float width, int height, float dirX, float dirY) {

        float xMin = x - radius;
        float xMax = x + radius;
        float yMin = y + radius;
        float yMax = y - radius;
        float minCoordinateX = 0;
        float maxCoordinateX = height - 1;
        float minCoordinateY = 0;
        float maxCoordinateY = width -1;

        if ( xMin < minCoordinateX ) {
        return new float[]{dirX,dirY};}
        else if ( xMax < maxCoordinateX) {
            return new float[] {dirX,dirY};}
        else if ( yMin < minCoordinateY) {
            return new float[]{dirX,dirY};}
        else if (yMax < maxCoordinateY) {
            return new float [] {dirX,dirY};}
        
        else
            return (float dirX, float dirY);}
    }

    /**
     *
     * @param x1 x-Wert des ersten Mittelpunkts
     * @param y1 y-Wert des ersten Mittelpunkts
     * @param x2 x-Wert des zweiten Mittelpunkts
     * @param y2 y-Wert des zweiten Mittlpunkts
     * @param dirX x-Wert des Richtungsvektors
     * @param dirY y-Wert des Richtungsvektors
     * @param attraction gegebener Anziehungswert
     * @return gibt neuen oder alten richtungsvertor zur�ck
     */
    public static float[] changeDirection(float x1, float y1, float x2, float y2, float dirX, float dirY, int attraction) {
        float tmp = (float) attraction;
        if(isCloser(x1, y1, x2, y2, tmp))
            return new float[]  {dirX,dirY};
        else {
            float distX = x2 - x1;
            float distY = y2 - y1;
            distX *= 0.04;
            distY *= 0.04;
            dirX += distX;
            dirY += distY;
            float norm = (float)Math.sqrt(Math.pow((double)dirX, 2)+ Math.pow((double)dirY, 2));
            float normaliser = 1 / norm;
            dirX *= normaliser;
            dirY *= normaliser;

            return  new float[]{ dirX, dirY};

        }
    }
}
